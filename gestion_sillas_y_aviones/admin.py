from django.contrib import admin

# Register your models here.
from .models import Origen, Destino, Ruta, Vuelo_disponible, Pelicula, Avion, Vuelo_asignado, Oferta, Cliente, Compra, Reembolso

admin.site.register(Origen)
admin.site.register(Destino)
admin.site.register(Ruta)
admin.site.register(Vuelo_disponible)
admin.site.register(Pelicula)
admin.site.register(Avion)
admin.site.register(Vuelo_asignado)
admin.site.register(Oferta)
admin.site.register(Cliente)
admin.site.register(Compra)
admin.site.register(Reembolso)