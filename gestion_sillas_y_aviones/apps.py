from django.apps import AppConfig


class GestionSillasYAvionesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gestion_sillas_y_aviones'
