# Generated by Django 3.2.8 on 2021-10-08 19:21

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('gestion_sillas_y_aviones', '0004_auto_20211008_1327'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rutas',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2021, 10, 8, 19, 21, 40, 970511, tzinfo=utc)),
        ),
    ]
