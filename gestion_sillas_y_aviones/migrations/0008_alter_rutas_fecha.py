# Generated by Django 3.2.8 on 2021-10-08 19:37

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('gestion_sillas_y_aviones', '0007_alter_rutas_fecha'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rutas',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2021, 10, 8, 19, 37, 41, 1485, tzinfo=utc), null=True),
        ),
    ]
