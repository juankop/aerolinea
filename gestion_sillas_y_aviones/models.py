from django.db import models
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.utils import timezone

# Create your models here.

class Origen(models.Model):
    id_origen=models.AutoField(primary_key=True)
    ciudad=models.CharField(max_length=15)
    pais=models.CharField(max_length=15)

class Destino(models.Model):
    id_destino=models.AutoField(primary_key=True)
    ciudad=models.CharField(max_length=15)
    pais=models.CharField(max_length=15)

class Ruta(models.Model):
    id_ruta=models.AutoField(primary_key=True)
    id_origen=models.ForeignKey(Origen, on_delete=CASCADE)
    id_destino=models.ForeignKey(Destino, on_delete=CASCADE)
    
class Vuelo_disponible(models.Model):
    id_vuelodisponible=models.AutoField(primary_key=True)
    id_ruta=models.ForeignKey(Ruta, on_delete=models.CASCADE)
    fecha=models.DateField()

class Pelicula(models.Model):
    id_pelicula=models.AutoField(primary_key=True)
    categoria=models.CharField(max_length=15)
    genero=models.CharField(max_length=15)
    año=models.IntegerField()

class Avion(models.Model):
    id_avion=models.AutoField(primary_key=True)
    id_pelicula=models.ForeignKey(Pelicula, on_delete=CASCADE)
    sillas=models.IntegerField()
    matricula=models.IntegerField()

class Vuelo_asignado(models.Model):
    id_vueloasignado=models.AutoField(primary_key=True)
    id_vuelodisponible=models.OneToOneField(Vuelo_disponible, on_delete=models.CASCADE)
    id_avion=models.ForeignKey(Avion, on_delete=CASCADE)
    costo=models.IntegerField()

class Oferta(models.Model):
    id_oferta=models.AutoField(primary_key=True)
    id_vueloasignado = models.ForeignKey(Vuelo_asignado, on_delete=models.CASCADE)
    porcentaje_descuento= models.DecimalField(max_digits = 3, decimal_places = 2)
#    imagen_destino= models.ImageField(Upload_to="photos")

class Cliente(models.Model):
    id_cliente=models.AutoField(primary_key=True)
    num_cuenta=models.IntegerField()
    cedula=models.CharField(max_length=15)
    nombre=models.CharField(max_length=15)
    apellido=models.CharField(max_length=15)
    fecha_nacimiento=models.DateField()

class Compra(models.Model):
    id_compra=models.AutoField(primary_key=True)
    id_vueloasignado = models.ForeignKey(Vuelo_asignado, on_delete=models.CASCADE)
    id_cliente=models.ForeignKey(Cliente, on_delete=models.CASCADE)
    id_oferta=models.ForeignKey(Oferta, on_delete=CASCADE)
    fecha_compra=models.DateField()

class Reembolso(models.Model):
    id_reembolso=models.AutoField(primary_key=True)
    id_compra=models.OneToOneField(Compra, on_delete=CASCADE)
    tipo_reembolso=models.CharField(max_length=15)
    tipo_motivo=models.CharField(max_length=15)
    estado_aprobacion=models.BooleanField()

